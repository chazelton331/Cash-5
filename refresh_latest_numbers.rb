#!/usr/bin/env ruby

require 'net/http'

begin
  fd      = File.open("latest-numbers.txt", 'w')
  url     = "http://www.state.nj.us/lottery/data/cash5.dat"
  uri     = URI.parse(url)
  http    = Net::HTTP.new(uri.host, uri.port)
  request = Net::HTTP::Get.new(uri.path)
  result  = http.request(request)
  fd.puts result.body
rescue => e
  puts "BOOM! #{e.message}"
  exit(1)
ensure
  fd.close
end

puts "Success."

