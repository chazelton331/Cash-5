#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>

#define LINE_SIZE 4096

int my_numbers[5];
int numbers[41] = { 0, 
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
int total_draws    = 0;
int numbers_search = 0;
int jackpot_wins   = 0;
int four_wins      = 0;
int three_wins     = 0;

void
usage(int status) {
  printf("usage: -f filename -n '01 02 03 31 32'\n");
  exit(status);
}

void
parse_numbers(FILE *file_pointer)
{
    char line[LINE_SIZE];
    char *line_ptr;

    int i, j, k;
    int number;
    int match_count;

    while (fgets(line, sizeof(line), file_pointer)) {
        match_count = 0;
        total_draws++;

        for( i = 0; i < 4; i++)
            line_ptr = strtok(((i) ? (NULL) : (line)), "%");

        for (j = 0; j < 5; j++) {
            line_ptr = strtok(NULL, "%");
            number = atoi(line_ptr);

            if (numbers_search)
                for (k = 0; k < 5; k++)
                    if (my_numbers[k] == number) match_count++;

            numbers[number]++;
        }

        if ((match_count == 5) && (numbers_search)) jackpot_wins++;
        if ((match_count == 4) && (numbers_search))    four_wins++;
        if ((match_count == 3) && (numbers_search))   three_wins++;

    }
}

float
percentage(int number, int draws)
{
    return(100 * ((float)number/(float)draws));
}

void
print_frequency(void)
{
    int i;

    for (i = 1; i < 41; i++)
        printf("%i hit %i times, %.3f%% frequency\n", i, numbers[i], percentage(numbers[i], total_draws));
}

void
print_total_draws(void)
{
    printf("total number of draws: %i\n", total_draws);
}

void
print_prize_winners(void)
{
    if (numbers_search) printf("the jackpot won this many times: %i\n", jackpot_wins);
    if (numbers_search) printf("the 4/5 won this many times: %i\n",     four_wins);
    if (numbers_search) printf("the 3/5 won this many times: %i\n",     three_wins);
}

void
print_sorted_numbers(void)
{
    puts("implement sorting");
}

void
print_stats(void)
{
    print_frequency();
    print_total_draws();
    print_prize_winners();
}


int
main(int argc, char *argv[])
{
    int opt;
    int sort = 0;
    char *filename;
    FILE *file_pointer;
    char *arg_ptr;

    int j;

    filename = NULL; 

    static struct option long_options[] = {
        { "filename", 1, 0, 'f' },
        { "numbers",  1, 0, 'n' },
        { "sort",     1, 0, 's' },
        { "help",     1, 0, 'h' },
        { 0,          0, 0,  0  }
    };

    while ((opt = getopt_long(argc, argv, "hf:n:s", long_options, NULL)) != -1) {
        switch (opt) {
            case 'h':
		usage(0);
            case 's':
                sort = 1;
                break;
            case 'f':
                filename = optarg;
                break;
            case 'n':
                numbers_search = 1;
                for( j = 0; j < 5; j++) {
                    arg_ptr = strtok(( (j) ? (NULL) : (optarg) ), " ");
                    if (arg_ptr)
                      my_numbers[j] = atoi(arg_ptr);
                }
                break;
            case '?':
                printf("Wrong argument, dude\n");
                break;
            default:
                printf("Boom\n");
                break;
        }
    }

    if (!filename) {
        usage(1);
    }

    if (!numbers_search) {
        usage(1);
    }

    if ((file_pointer = fopen(filename, "r")) == NULL) {
        printf("Error trying to read file %s\n", filename);
        usage(1);
    }

    parse_numbers(file_pointer);

    print_stats();

    if (sort) print_sorted_numbers();

    return(0);
}
