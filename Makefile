LOC  	 = /usr/local/bin

cash5: cash5.o cash5.h
	gcc cash5.o -o cash5
	rm cash5.o

cash5.o: cash5.c cash5.h
	gcc -c cash5.c

all: cash5 install clean

install: cash5
	sudo mkdir -p $(LOC)
	sudo cp cash5 $(LOC)
	sudo chmod 0755 $(LOC)/cash5

clean:
	sudo rm -f *.o *.s a.out cash5

uninstall: clean
	sudo rm -f $(LOC)/cash5
